package evaluator.controller;

import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AppControllerTestLab3 {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    private Intrebare intrebare1;
    private Intrebare intrebare2;
    private Intrebare intrebare3;
    private Intrebare intrebare4;
    private Intrebare intrebare5;
    private Intrebare intrebare6;
    private Intrebare intrebare7;
    private Intrebare intrebare8;
    private Intrebare intrebare9;
    private Intrebare intrebare10;
    private AppController service;
    private List<Intrebare> intrebari;


    @Before
    public void setUp() throws Exception {
        intrebare1 = new Intrebare("Ce?", "1)q", "2)w", "3)e", "3", "Biologie");
        intrebare2 = new Intrebare("Cee?", "1)a", "2)s", "3)d", "1", "Geografie");
        intrebare3 = new Intrebare("Ceee?", "1)a", "2)s", "3)d", "1", "Stuff1");
        intrebare4 = new Intrebare("Ceeee?", "1)a", "2)s", "3)d", "1", "Stuff2");
        intrebare5 = new Intrebare("Ceeeee?", "1)a", "2)s", "3)d", "1", "Stuff3");
        intrebare6 = new Intrebare("Ceee?", "1)a", "2)s", "3)d", "1", "Stuff4");
        intrebare7 = new Intrebare("Ceeee?", "1)a", "2)s", "3)d", "1", "Stuff5");
        intrebare8 = new Intrebare("Ceee?", "1)a", "2)s", "3)d", "1", "Stuff5");
        intrebare9 = new Intrebare("Ceee?", "1)a", "2)s", "3)d", "1", "Stuff6");
        intrebare10 = new Intrebare("Cee?", "1)a", "2)s", "3)d", "1", "Stuff6");
        service = new AppController();
        intrebari = new ArrayList<>();
    }


    @Test
    public void testCase1() {
        try {
            service.createNewTest(intrebari);
        } catch (NotAbleToCreateTestException e) {
            assertEquals(e.getMessage(),"Nu exista suficiente intrebari pentru crearea unui test!(5)");
        }
    }

    @Test
    public void testCase2() {
        try {
            intrebari.add(intrebare1);
            intrebari.add(intrebare2);
            intrebari.add(intrebare3);
            intrebari.add(intrebare9);
            intrebari.add(intrebare10);
            service.createNewTest(intrebari);
        } catch (NotAbleToCreateTestException e) {
            assertEquals(e.getMessage(),"Nu exista suficiente domenii pentru crearea unui test!(5)");
        }
    }

    @Test
    public void testCase3() {
        evaluator.model.Test test = null;
        try {
            intrebari.clear();
            intrebari.add(intrebare1);
            intrebari.add(intrebare2);
            intrebari.add(intrebare3);
            intrebari.add(intrebare9);
            intrebari.add(intrebare4);
            test = service.createNewTest(intrebari);
        } catch (NotAbleToCreateTestException e) {
            assert true;
        }
        assertEquals(test.getIntrebari().size(),5);
    }

    @Test
    public void testCase4() {
        evaluator.model.Test test = null;
        try {
            intrebari.clear();
            intrebari.add(intrebare1);
            intrebari.add(intrebare2);
            intrebari.add(intrebare3);
            intrebari.add(intrebare4);
            intrebari.add(intrebare5);
            intrebari.add(intrebare6);
            intrebari.add(intrebare7);
            intrebari.add(intrebare8);
            intrebari.add(intrebare9);
            intrebari.add(intrebare10);
            test = service.createNewTest(intrebari);
        } catch (NotAbleToCreateTestException e) {
            assert true;
        }
        assertEquals(test.getIntrebari().size(),5);
    }
}
