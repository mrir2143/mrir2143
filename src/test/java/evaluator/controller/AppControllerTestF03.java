package evaluator.controller;

import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.util.Statistica;
import org.junit.Test;

import static org.junit.Assert.*;

public class AppControllerTestF03 {

    @Test
    public void statisticTest1() throws Exception{
        AppController appController = new AppController();
        appController.loadIntrebariFromFile("D:\\2V2S\\5-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt");
        Statistica statistica =appController.getStatistica(appController.getAll());
        assert(statistica.getIntrebariDomenii().size()==5);

    }
    @Test(expected = NotAbleToCreateStatisticsException.class)
    public void statisticTest2() throws Exception{
        AppController appController = new AppController();
        appController.loadIntrebariFromFile("D:\\2V2S\\5-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari2.txt");
        Statistica statistica =appController.getStatistica(appController.getAll());

    }
}