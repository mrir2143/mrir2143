package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static junit.framework.TestCase.assertEquals;


public class AppControllerTest {


    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    private Intrebare intrebare1;
    private Intrebare intrebare2;
    private AppController service;
    private String enunt;
    private String enunt2;

    @Before
    public void setUp() throws Exception {
        intrebare1 = new Intrebare("Ceva?","1)q","2)w","3)e","3","Biologie");
        intrebare2 = new Intrebare("altceva?","1)a","2)s","3)d","1","Geografie");
        service = new AppController();
        enunt="C";
        for(int i=1;i<100;i++){
            enunt+="m";
        }
        enunt+="?";

        enunt2="M";
        for(int i=1;i<99;i++){
            enunt2+="k";
        }
        enunt2+="?";
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected=InputValidationFailedException.class)
    public void add1() throws DuplicateIntrebareException, InputValidationFailedException {
        //exceptionRule.expect(InputValidationFailedException.class);
        intrebare1.setVariantaCorecta("7");
        service.addNewIntrebare(intrebare1);
        assertEquals(service.exists(intrebare1),false);
    }

    @Test(expected=InputValidationFailedException.class)
    public void add2() throws DuplicateIntrebareException, InputValidationFailedException {
        //exceptionRule.expect(InputValidationFailedException.class);
        service.addNewIntrebare(intrebare2);
        assertEquals(false,service.exists(intrebare2));
    }

    @Test
    public void add3() throws DuplicateIntrebareException, InputValidationFailedException {
        service.addNewIntrebare(intrebare1);
        assertEquals(service.exists(intrebare1),true);
    }

    @Test(expected=InputValidationFailedException.class)
    public void add4() throws DuplicateIntrebareException, InputValidationFailedException {
        //exceptionRule.expect(InputValidationFailedException.class);
        intrebare1.setEnunt(enunt);
        service.addNewIntrebare(intrebare1);
        assertEquals(service.exists(intrebare1),false);
    }

    @Test(expected=InputValidationFailedException.class)
    public void add5() throws DuplicateIntrebareException, InputValidationFailedException {
        //exceptionRule.expect(InputValidationFailedException.class);
        intrebare1.setEnunt("M");
        service.addNewIntrebare(intrebare1);
        assertEquals(service.exists(intrebare1),false);
    }

    @Test
    public void add6() throws DuplicateIntrebareException, InputValidationFailedException {
        intrebare1.setEnunt("M?");
        service.addNewIntrebare(intrebare1);
        assertEquals(service.exists(intrebare1),true);
    }

    @Test
    public void add7() throws DuplicateIntrebareException, InputValidationFailedException {
        intrebare1.setEnunt(enunt2);
        service.addNewIntrebare(intrebare1);
        assertEquals(service.exists(intrebare1),true);
    }
}