package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.util.Statistica;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class BigBang {
    private AppController appController;

    @Before
    public void setUp() throws Exception {
        appController = new AppController();
    }

    @Test
    public void addQuestionTest1() throws Exception {
        appController.addNewIntrebare(new Intrebare("In ce zi ne aflam?", "1)Sambata", "2)Duminica", "3)Luni", "3", "Zile"));

    }

    @Test
    public void createNewTest2() throws Exception {
        appController.loadIntrebariFromFile("D:\\2V2S\\5-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt");
        evaluator.model.Test t = appController.createNewTest(appController.getAll());
        assertEquals(t.getIntrebari().size(), 5);
    }

    @Test
    public void statisticTest1() throws Exception {
        appController.loadIntrebariFromFile("D:\\2V2S\\5-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt");
        Statistica statistica = appController.getStatistica(appController.getAll());
        assert (statistica.getIntrebariDomenii().size() == 5);

    }

    @Test
    public void integrationTest() throws InputValidationFailedException, DuplicateIntrebareException, IOException, NotAbleToCreateTestException, NotAbleToCreateStatisticsException {
        // try{




        appController.loadIntrebariFromFile("D:\\2V2S\\5-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt");
        evaluator.model.Test t = appController.createNewTest(appController.getAll());
        assertEquals(t.getIntrebari().size(), 5);


        appController.loadIntrebariFromFile("D:\\2V2S\\5-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt");
        appController.addNewIntrebare(new Intrebare("In ce zi ne aflam?", "1)Sambata", "2)Duminica", "3)Luni", "3", "Zile"));
        appController.addNewIntrebare(new Intrebare("In ce zi ne aflam maine?", "1)Sambata", "2)Duminica", "3)Luni", "3", "Zile"));
        Statistica statistica = appController.getStatistica(appController.getAll());
        assert (statistica.getIntrebariDomenii().size() == 6);

    }
}