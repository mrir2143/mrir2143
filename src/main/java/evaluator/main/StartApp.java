package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Test;
import evaluator.util.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;

//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "D:\\2V2S\\5-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt";
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		
		AppController appController = new AppController();
		
		boolean activ = true;
		String optiune = null;
        appController.loadIntrebariFromFile(file);
		
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			
			optiune = console.readLine();
			
			switch(optiune){
			case "1" :
				String enunt,var1,var2,var3,varCorect,domeniu;
				Scanner in = new Scanner(System.in);
				System.out.println("Dati enuntul: ");
				enunt=in.nextLine();
				System.out.println("Dati primul raspuns: ");
				var1=in.nextLine();
				System.out.println("r2: ");
				var2=in.nextLine();
				System.out.println("r3: ");
				var3=in.nextLine();
				System.out.println("r corect: ");
				varCorect=in.nextLine();
				System.out.println("Domeniu: ");
				domeniu=in.nextLine();
				Intrebare intr = null;
				try {
					intr = new Intrebare(enunt,var1,var2,var3,varCorect,domeniu);
				} catch (InputValidationFailedException e) {
					System.out.println(e.getMessage());
				}
				try {
					appController.addNewIntrebare(intr);
				} catch (DuplicateIntrebareException e) {
					System.out.println(e.getMessage());
				} catch (InputValidationFailedException e) {
					System.out.println(e.getMessage());
				}
				break;

				case "2" :
					try {
						Test test = appController.createNewTest(appController.getAll());
						List<Intrebare> intrebari = test.getIntrebari();
						for(Intrebare x:intrebari){
                            System.out.println(x);
                        }
					} catch (NotAbleToCreateTestException e) {
						System.out.println(e.getMessage());
					}
					break;
				case "3" :
				Statistica statistica;
				try {
					statistica = appController.getStatistica(appController.getAll());
					System.out.println(statistica);
				} catch (NotAbleToCreateStatisticsException e) {
					// TODO 
				}
				
				break;
			case "4" :
				activ = false;
				break;
			default:
				break;
			}
		}
		
	}

}
